﻿using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Realms;

namespace Cocktail_App.Models
{
    public class CocktailModel:RealmObject
    {

        public string strDrink { get; set; }
        public string strDrinkThumb { get; set; }
        public string idDrink { get; set; }

        public string strCategory { get; set; }
        public string strAlcoholic { get; set; }
        public string strGlass { get; set; }
        public string strIBA { get; set; }
        

        public string strInstructions   { get; set; }

        public string strIngredient1 { get; set; }
        public string strIngredient2 { get; set; }
        public string strIngredient3 { get; set; }
        public string strIngredient4 { get; set; }
        public string strIngredient5 { get; set; }
        public string strIngredient6 { get; set; }


        public string strMeasure1 { get; set; }
        public string strMeasure2 { get; set; }
        public string strMeasure3 { get; set; }
        public string strMeasure4 { get; set; }
        public string strMeasure5 { get; set; }
        public string strMeasure6 { get; set; }



        public CocktailModel()
        {
        }


        public async static Task<ObservableCollection<CocktailModel>> GetAllCocktailbyCategory(string id_category, string category)
        {
            using (HttpClient client = new HttpClient())
            {
                var uri = new Uri("https://www.thecocktaildb.com/api/json/v1/1/filter.php?"+category+"="+id_category);

                HttpResponseMessage response = await client.GetAsync(uri).ConfigureAwait(false);

                string answer = await response.Content.ReadAsStringAsync();

                ResponseCocktailModel responseObject = JsonConvert.DeserializeObject<ResponseCocktailModel>(answer);

                return responseObject.drinks;

            }
        }

        public async static Task<ObservableCollection<CocktailModel>> GetCocktailDetail(String idCocktail)
        {
            using (HttpClient client = new HttpClient())
            {
                var uri = new Uri("https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=" + idCocktail);

                HttpResponseMessage response = await client.GetAsync(uri).ConfigureAwait(false);

                string answer = await response.Content.ReadAsStringAsync();

                ResponseCocktailModel responseObject = JsonConvert.DeserializeObject<ResponseCocktailModel>(answer);

                return responseObject.drinks;

            }
        }



    }
}
