﻿using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Realms;

namespace Cocktail_App.Models
{
    public class CategoriesModel:RealmObject
    {
        [PrimaryKey]
        public string strCategory { get; set; }


        public CategoriesModel()
        {
        }


        public async static Task<ObservableCollection<CategoriesModel>> GetAllGategories()
        {
            using (HttpClient client = new HttpClient())
            {
                var uri = new Uri("https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list");

                HttpResponseMessage response = await client.GetAsync(uri).ConfigureAwait(false);

                string answer = await response.Content.ReadAsStringAsync();

                ResponseCategoryModel responseObject = JsonConvert.DeserializeObject<ResponseCategoryModel>(answer);

                return responseObject.drinks;

            }



        }




    }
}
