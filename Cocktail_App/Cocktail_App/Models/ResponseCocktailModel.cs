﻿using System;
using System.Collections.ObjectModel;

namespace Cocktail_App.Models
{
    public class ResponseCocktailModel
    {

        public ObservableCollection<CocktailModel> drinks { get; set; }

        public ResponseCocktailModel()
        {
        }
    }
}
