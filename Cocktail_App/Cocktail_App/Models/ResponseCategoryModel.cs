﻿using System;
using System.Collections.ObjectModel;

namespace Cocktail_App.Models
{
    public class ResponseCategoryModel
    {

        public ObservableCollection<CategoriesModel> drinks { get; set; }

        public ResponseCategoryModel()
        {
        }
    }
}
