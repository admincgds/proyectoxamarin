﻿using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Realms;

namespace Cocktail_App.Models
{
    public class GlasessModel:RealmObject
    {

        public string strGlass { get; set; }

        public GlasessModel()
        {
        }

        public async static Task<ObservableCollection<GlasessModel>> GetAllGlasses()
        {
            using (HttpClient client = new HttpClient())
            {
                var uri = new Uri("https://www.thecocktaildb.com/api/json/v1/1/list.php?g=list");

                HttpResponseMessage response = await client.GetAsync(uri).ConfigureAwait(false);

                string answer = await response.Content.ReadAsStringAsync();

                ResponseGlassModel responseObject = JsonConvert.DeserializeObject<ResponseGlassModel>(answer);

                return responseObject.drinks;

            }
        }



    }
}
