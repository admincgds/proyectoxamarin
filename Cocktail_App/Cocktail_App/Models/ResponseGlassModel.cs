﻿using System;
using System.Collections.ObjectModel;

namespace Cocktail_App.Models
{
    public class ResponseGlassModel
    {

        public ObservableCollection<GlasessModel> drinks { get; set; }

        public ResponseGlassModel()
        {
        }
    }
}
