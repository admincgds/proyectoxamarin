﻿using System;
using System.Collections.Generic;
using Cocktail_App.ViewModels;
using Xamarin.Forms;

namespace Cocktail_App.Views
{
    public partial class CocktailView : ContentPage
    {
        public CocktailView(string idCocktail, string filtro)
        {
            InitializeComponent();
            BindingContext = new CocktailViewModel(idCocktail,filtro);
            this.Title = idCocktail;
        }
    }
}
