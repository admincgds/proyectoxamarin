﻿using System;
using System.Collections.Generic;
using Cocktail_App.ViewModels;
using Xamarin.Forms;

namespace Cocktail_App.Views
{
    public partial class CocktailDetailView : ContentPage
    {
        public CocktailDetailView(string idCocktail)
        {
            InitializeComponent();
            BindingContext = new CocktailDetailViewModel(idCocktail);
        }
    }
}
