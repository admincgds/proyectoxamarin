﻿using System;
using System.Collections.Generic;
using Cocktail_App.ViewModels;
using Xamarin.Forms;

namespace Cocktail_App.Views
{
    public partial class CategoriesView : ContentPage
    {
        public CategoriesView()
        {
            InitializeComponent();
            BindingContext = CategoriesViewModel.GetInstance();
        }
    }
}
