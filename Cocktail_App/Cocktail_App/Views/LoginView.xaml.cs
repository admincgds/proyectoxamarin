﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cocktail_App.DataBase;
using Cocktail_App.ViewModels;
using Realms;
using Xamarin.Forms;

namespace Cocktail_App.Views
{
    public partial class LoginView : ContentPage
    {
        public LoginView()
        {
            InitializeComponent();
            BindingContext = LoginViewModel.GetInstance();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            VerifyUserLogged();
        }


        private async void VerifyUserLogged()
        {
            var realm = Realm.GetInstance();

            User bdUser = realm.All<User>().Where(u => u.Remember == true).FirstOrDefault();

            if (bdUser != null)
            {
                LoginViewModel.GetInstance().user = bdUser;
                await Shell.Current.GoToAsync("//MainPage");
            }
        }
    }
}
