﻿using System;
using System.Collections.Generic;
using Cocktail_App.ViewModels;
using Xamarin.Forms;

namespace Cocktail_App.Views
{
    public partial class GlassesView : ContentPage
    {
        public GlassesView()
        {
            InitializeComponent();

            BindingContext = GlassesViewModel.GetInstance();
        }
    }
}
