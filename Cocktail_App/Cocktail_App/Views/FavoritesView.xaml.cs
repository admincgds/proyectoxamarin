﻿using System;
using System.Collections.Generic;
using Cocktail_App.ViewModels;
using Xamarin.Forms;

namespace Cocktail_App.Views
{
    public partial class FavoritesView : ContentPage
    {
        public FavoritesView()
        {
            InitializeComponent();
            BindingContext = new CocktailViewModel();
         
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindingContext = new CocktailViewModel();
        }

    }
}
