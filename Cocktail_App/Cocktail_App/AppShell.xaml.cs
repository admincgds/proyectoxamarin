﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cocktail_App.DataBase;
using Cocktail_App.ViewModels;
using Cocktail_App.Views;
using Realms;
using Xamarin.Forms;

namespace Cocktail_App
{
    public partial class AppShell : Shell
    {

        private User user_logged = new User();

        public AppShell()
        {
            InitializeComponent();
            RegisterRoutes();
            BindingContext = LoginViewModel.GetInstance();
        }

        private void RegisterRoutes()
        {
            Routing.RegisterRoute("SignUpPage", typeof(RegisterView));
            Routing.RegisterRoute("LoginPage", typeof(LoginView));
            Routing.RegisterRoute("MainPage", typeof(CategoriesView));
        }


    }
}
