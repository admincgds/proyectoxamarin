﻿using System;
using System.Collections.Generic;
using Cocktail_App.Models;
using Realms;

namespace Cocktail_App.DataBase
{
    public class User : RealmObject
    {
        [PrimaryKey]
        public string Email { get; set; }
        public string Fullname { get; set; }
        public string Password { get; set; }
        public string StrImage { get; set; }
        public byte[] BtImage { get; set; }
        public bool Session { get; set; }
        public bool Remember { get; set; }
        public IList<CocktailModel> favoriteCocktailList { get; }

        public User()
        {
            
        }
    }
}
