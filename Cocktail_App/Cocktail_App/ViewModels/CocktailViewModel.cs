﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Cocktail_App.DataBase;
using Cocktail_App.Models;
using Cocktail_App.Views;
using Realms;
using Xamarin.Forms;

namespace Cocktail_App.ViewModels
{
    public class CocktailViewModel : INotifyPropertyChanged
    {


        private ObservableCollection<CocktailModel> _lstCocktail = new ObservableCollection<CocktailModel>();

        public ObservableCollection<CocktailModel> lstCocktail
        {
            get
            {
                return _lstCocktail;
            }
            set
            {
                _lstCocktail = value;
                OnPropertyChanged("lstCocktail");
            }
        }



        private CocktailModel _CurrentCocktail;

        public CocktailModel CurrentCocktail
        {
            get
            {
                return _CurrentCocktail;
            }

            set
            {
                _CurrentCocktail = value;
                OnPropertyChanged("CurrentCocktail");
            }
        }


        public CocktailViewModel(string id_category, string category)
        {
            InitClass(id_category, category);
            InitCommand();
        }

        public CocktailViewModel()
        {
            InitClassFavorite();
            InitCommand();
        }



        public async void InitClass(string id_category, string category)
        {
            try
            {
                var realm = Realm.GetInstance();
                var cocktails = realm.All<CocktailModel>().Where(c => c.strCategory == id_category).ToList();
                if (cocktails.Count == 0)
                {
                    lstCocktail = await CocktailModel.GetAllCocktailbyCategory(id_category, category);
                    for (int i = 0; i < lstCocktail.Count; i++)
                    {
                        realm.Write(() =>
                        {
                            realm.Add(new CocktailModel
                            {
                                strDrink = lstCocktail[i].strDrink,
                                strDrinkThumb = lstCocktail[i].strDrinkThumb,
                                idDrink = lstCocktail[i].idDrink,
                                strCategory = id_category,
                                strAlcoholic = lstCocktail[i].strAlcoholic,
                                strGlass = lstCocktail[i].strGlass,
                                strIBA = lstCocktail[i].strIBA,
                                strInstructions = lstCocktail[i].strInstructions,
                                strIngredient1 = lstCocktail[i].strIngredient1,
                                strIngredient2 = lstCocktail[i].strIngredient2,
                                strIngredient3 = lstCocktail[i].strIngredient3,
                                strIngredient4 = lstCocktail[i].strIngredient4,
                                strIngredient5 = lstCocktail[i].strIngredient5,
                                strIngredient6 = lstCocktail[i].strIngredient6,
                                strMeasure1 = lstCocktail[i].strMeasure1,
                                strMeasure2 = lstCocktail[i].strMeasure2,
                                strMeasure3 = lstCocktail[i].strMeasure3,
                                strMeasure4 = lstCocktail[i].strMeasure4,
                                strMeasure5 = lstCocktail[i].strMeasure5,
                                strMeasure6 = lstCocktail[i].strMeasure6

                            });
                        });

                    }
                }
                else
                {
                    for (int i = 0; i < cocktails.Count; i++)
                    {
                        lstCocktail.Add(cocktails[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Información", ex.ToString(), "OK");
            }
        }

        public async void InitClassFavorite()
        {
            try
            {
                var user = LoginViewModel.GetInstance().user;
                var realm = Realm.GetInstance();
                var userDB = realm.All<User>().Where(u => u.Email == user.Email).FirstOrDefault();
                if (userDB.favoriteCocktailList.Count()==0)
                {
                    await Application.Current.MainPage.DisplayAlert("Información", "No hay cockteles agregados como favoritos...", "Entendido");
                }
                else
                {
                    for (int i = 0; i < userDB.favoriteCocktailList.Count; i++)
                    {
                        lstCocktail.Add(userDB.favoriteCocktailList[i]);
                    }
                }

            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Información", ex.ToString(), "OK");
            }
        }

        #region Comandos

        public ICommand GetCocktailDetailCommand { get; set; }

        #endregion

        public void InitCommand()
        {
            //GetCocktailDetailCommand = new Command(async () => await PushPage(new CocktailDetailView(CurrentCocktail.idDrink)));
            GetCocktailDetailCommand = new Command(GetCocktailDetail);
        }


        private async Task PushPage(Page page)
        {
            await Shell.Current.Navigation.PushAsync(page);
            Shell.Current.FlyoutIsPresented = false;
        }


        private async void GetCocktailDetail()
        {
            try
            {
                string id_drink = CurrentCocktail.idDrink;
                await Shell.Current.Navigation.PushAsync(new CocktailDetailView(id_drink));
                Shell.Current.FlyoutIsPresented = false;
                CurrentCocktail = null;
            }
            catch (Exception ex)
            {

            }
        }




        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (propertyName != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }

}
