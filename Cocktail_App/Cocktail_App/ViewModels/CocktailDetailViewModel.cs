﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Cocktail_App.DataBase;
using Cocktail_App.Models;
using Realms;
using Xamarin.Forms;

namespace Cocktail_App.ViewModels
{
    public class CocktailDetailViewModel : INotifyPropertyChanged
    {

        private ObservableCollection<CocktailModel> _lstCocktailDetail = new ObservableCollection<CocktailModel>();
        private string _textoBoton;
        public ObservableCollection<CocktailModel> lstCocktailDetail
        {
            get
            {
                return _lstCocktailDetail;
            }
            set
            {
                _lstCocktailDetail = value;
                OnPropertyChanged("lstCocktailDetail");
            }
        }

        private CocktailModel _CurrentCocktailDetail = new CocktailModel();

        public CocktailModel CurrentCocktailDetail
        {
            get
            {
                return _CurrentCocktailDetail;
            }

            set
            {
                _CurrentCocktailDetail = value;
                OnPropertyChanged("CurrentCocktailDetail");
            }
        }

        public string textoBoton
        {
            get
            {
                return _textoBoton;
            }

            set
            {
                _textoBoton = value;
                OnPropertyChanged("textoBoton");
            }
        }

        #region Comandos

        public ICommand FavoriteCommand { get; set; }

        #endregion



        public CocktailDetailViewModel(string id_drink)
        {

            InitClass(id_drink);
            InitCommand();

        }


        public async void InitClass(string id_drink)
        {
            try
            {
                var realm = Realm.GetInstance();
                var cocktail = realm.All<CocktailModel>().Where(c => c.idDrink == id_drink).FirstOrDefault();
                var user = LoginViewModel.GetInstance().user;
                var userDB = realm.All<User>().Where(u => u.Email == user.Email).FirstOrDefault();
                var cocktailExist = (userDB.favoriteCocktailList != null) ? userDB.favoriteCocktailList.Where(c => c.idDrink == id_drink).FirstOrDefault() : null;
                if (cocktailExist != null)
                {
                    _textoBoton = "Eliminar favorito";
                }
                else
                {
                    _textoBoton = "Agregar favorito";
                }
                if (cocktail.strIngredient1 == null)
                {
                    lstCocktailDetail = await CocktailModel.GetCocktailDetail(id_drink);
                    CurrentCocktailDetail = lstCocktailDetail.FirstOrDefault();
                    for (int i = 0; i < lstCocktailDetail.Count; i++)
                    {
                        realm.Write(() =>
                        {

                            cocktail.strDrink = lstCocktailDetail[i].strDrink;
                            cocktail.strDrinkThumb = lstCocktailDetail[i].strDrinkThumb;
                            cocktail.idDrink = lstCocktailDetail[i].idDrink;
                            cocktail.strCategory = lstCocktailDetail[i].strCategory;
                            cocktail.strAlcoholic = lstCocktailDetail[i].strAlcoholic;
                            cocktail.strGlass = lstCocktailDetail[i].strGlass;
                            cocktail.strIBA = lstCocktailDetail[i].strIBA;
                            cocktail.strInstructions = lstCocktailDetail[i].strInstructions;
                            cocktail.strIngredient1 = lstCocktailDetail[i].strIngredient1;
                            cocktail.strIngredient2 = lstCocktailDetail[i].strIngredient2;
                            cocktail.strIngredient3 = lstCocktailDetail[i].strIngredient3;
                            cocktail.strIngredient4 = lstCocktailDetail[i].strIngredient4;
                            cocktail.strIngredient5 = lstCocktailDetail[i].strIngredient5;
                            cocktail.strIngredient6 = lstCocktailDetail[i].strIngredient6;
                            cocktail.strMeasure1 = lstCocktailDetail[i].strMeasure1;
                            cocktail.strMeasure2 = lstCocktailDetail[i].strMeasure2;
                            cocktail.strMeasure3 = lstCocktailDetail[i].strMeasure3;
                            cocktail.strMeasure4 = lstCocktailDetail[i].strMeasure4;
                            cocktail.strMeasure5 = lstCocktailDetail[i].strMeasure5;
                            cocktail.strMeasure6 = lstCocktailDetail[i].strMeasure6;

                        });

                    }
                }
                else
                {
                    lstCocktailDetail.Add(cocktail);
                    //REVISAR ESTA LINEA SE VA AL EXCEPTION
                    CurrentCocktailDetail = cocktail;
                   
                }

               
            }
            catch (Exception ex)
            {
                //await Application.Current.MainPage.DisplayAlert("Información", ex.ToString(), "OK");
            }
            //await Application.Current.MainPage.DisplayAlert("Error", id_drink, "OK");
        }

        public void InitCommand()
        {
            //GetCocktailByCategoriesCommand = new Command(async () => await PushPage(new CocktailView(CurrentCategory.strCategory, "c")));
            FavoriteCommand = new Command(favoriteCommand);
        }

        private async void favoriteCommand() {
            try
            {
                var user =  LoginViewModel.GetInstance().user;
                var realm = Realm.GetInstance();
                var userDB = realm.All<User>().Where(u => u.Email == user.Email).FirstOrDefault();
                var cocktail = (userDB.favoriteCocktailList != null) ? userDB.favoriteCocktailList.Where(c => c.idDrink == CurrentCocktailDetail.idDrink).FirstOrDefault() : null;
                if (cocktail!=null)
                {
                    realm.Write(() =>
                    {
                        userDB.favoriteCocktailList.Remove(cocktail);
                    });
                    textoBoton = "Agregar favorito";
                    // user.favoriteCocktailList.Remove(CurrentCocktailDetail);
                    await Application.Current.MainPage.DisplayAlert("Información", "Este cocktel ha sido eliminado como favorito", "Entendido");
                }
                else
                {
                    realm.Write(() =>
                    {
                      
                        userDB.favoriteCocktailList.Add(CurrentCocktailDetail);
                    });
                    textoBoton = "Eliminar favorito";
                    await Application.Current.MainPage.DisplayAlert("Información", "Este cocktel ha sido agregado como favorito", "Entendido");
                }
            }
            catch (Exception ex) {
                await Application.Current.MainPage.DisplayAlert("Información",ex.ToString(),"Entendido");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (propertyName != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
