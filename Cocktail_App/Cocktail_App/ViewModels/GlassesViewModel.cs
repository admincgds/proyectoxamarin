﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Cocktail_App.Models;
using Cocktail_App.Views;
using Realms;
using Xamarin.Forms;
namespace Cocktail_App.ViewModels
{
    public class GlassesViewModel : INotifyPropertyChanged
    {

        #region Propiedades
        private ObservableCollection<GlasessModel> _lstGlasses = new ObservableCollection<GlasessModel>();

        public ObservableCollection<GlasessModel> lstGlasses
        {
            get
            {
                return _lstGlasses;
            }
            set
            {
                _lstGlasses = value;
                OnPropertyChanged("lstGlasses");
            }
        }
        #endregion

        private GlasessModel _CurrentGlass;

        public GlasessModel CurrentGlass
        {
            get
            {
                return _CurrentGlass;
            }

            set
            {
                _CurrentGlass = value;
                OnPropertyChanged("CurrentGlass");
            }
        }


        #region Singleton
        private static GlassesViewModel instance = null;

        private GlassesViewModel()
        {
            InitClass();
            InitCommand();
        }

        public static GlassesViewModel GetInstance()
        {
            if (instance == null)
            {
                instance = new GlassesViewModel();
            }

            return instance;
        }

        public static void DeleteInstance()
        {
            if (instance != null)
            {
                instance = null;
            }
        }
        #endregion


        #region Comandos

        public ICommand GetCocktailByGlassCommand { get; set; }

        #endregion


        #region Metodos

        public async void InitClass()
        {
            try
            {
                var realm = Realm.GetInstance();
                var glasses = realm.All<GlasessModel>().ToList();
                if (glasses.Count == 0)
                {
                    lstGlasses = await GlasessModel.GetAllGlasses();
                    for (int i = 0; i < lstGlasses.Count; i++)
                    {
                        realm.Write(() =>
                        {
                            realm.Add(new GlasessModel { strGlass = lstGlasses[i].strGlass });
                        });

                    }
                }
                else {
                    for (int i = 0; i < glasses.Count(); i++) {
                        lstGlasses.Add(glasses[i]);
                    }

                }
            }
            catch (Exception ex) {
                await Application.Current.MainPage.DisplayAlert("Información",ex.ToString(),"OK");
            }

        }

        public void InitCommand()
        {
            //GetCocktailByGlassCommand = new Command(async () => await PushPage(new CocktailView(_CurrentGlass.strGlass, "g")));
            GetCocktailByGlassCommand = new Command(GetCocktailByGlass);
        }



        private async Task PushPage(Page page)
        {
            await Shell.Current.Navigation.PushAsync(page);
            Shell.Current.FlyoutIsPresented = false;
        }

        private async void GetCocktailByGlass()
        {
            try
            {
                string glass = CurrentGlass.strGlass;
                await Shell.Current.Navigation.PushAsync(new CocktailView(glass, "g"));
                Shell.Current.FlyoutIsPresented = false;
                CurrentGlass = null;
            }
            catch (Exception ex)
            {

            }
        }


        #endregion 



        public event PropertyChangedEventHandler PropertyChanged;


        protected void OnPropertyChanged(string propertyName)
        {
            if (propertyName != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
