﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Cocktail_App.Models;
using Cocktail_App.Views;
using Realms;
using Xamarin.Forms;

namespace Cocktail_App.ViewModels
{
    public class CategoriesViewModel : INotifyPropertyChanged
    {


        #region Propiedades
        private ObservableCollection<CategoriesModel> _lstCategories = new ObservableCollection<CategoriesModel>();

        public ObservableCollection<CategoriesModel> lstCategories
        {
            get
            {
                return _lstCategories;
            }
            set
            {
                _lstCategories = value;
                OnPropertyChanged("lstCategories");
            }
        }
        #endregion


        private CategoriesModel _CurrentCategory = new CategoriesModel();

        public CategoriesModel CurrentCategory
        {
            get
            {
                return _CurrentCategory;
            }

            set
            {
                _CurrentCategory = value;
                OnPropertyChanged("CurrentCategory");
            }
        }

        #region Singleton
        private static CategoriesViewModel instance = null;

        public CategoriesViewModel()
        {
            InitClass();
            InitCommand();
        }

        public static CategoriesViewModel GetInstance()
        {
            if (instance == null)
            {
                instance = new CategoriesViewModel();
            }

            return instance;
        }

        public static void DeleteInstance()
        {
            if (instance != null)
            {
                instance = null;
            }
        }



        #endregion

        #region Comandos

        public ICommand GetCocktailByCategoriesCommand { get; set; }

        #endregion


        #region Metodos

        public async void InitClass()
        {
            try
            {
                var realm = Realm.GetInstance();
                var categories = realm.All<CategoriesModel>().ToList();
                if (categories.Count == 0)
                {
                    lstCategories = await CategoriesModel.GetAllGategories();
                    for (int i = 0; i < lstCategories.Count; i++)
                    {
                        realm.Write(() =>
                        {
                            realm.Add(new CategoriesModel { strCategory = lstCategories[i].strCategory });
                        });

                    }
                }
                else
                {
                    for (int i = 0; i < categories.Count; i++)
                    {
                        lstCategories.Add(categories[i]);
                    }
                   
                }
            }
            catch (Exception ex) {
                await Application.Current.MainPage.DisplayAlert("Información", ex.ToString(), "OK");
            }
        }

        public void InitCommand()
        {
            //GetCocktailByCategoriesCommand = new Command(async () => await PushPage(new CocktailView(CurrentCategory.strCategory, "c")));
            GetCocktailByCategoriesCommand = new Command(GetCocktailByCategories);
        }



        private async Task PushPage(Page page)
        {
            await Shell.Current.Navigation.PushAsync(page);
            Shell.Current.FlyoutIsPresented = false;
        }

        private async void GetCocktailByCategories()
        {
            try
            {
                string category = CurrentCategory.strCategory;
                await Shell.Current.Navigation.PushAsync(new CocktailView(category, "c"));
                Shell.Current.FlyoutIsPresented = false;
                CurrentCategory = null;
            }
            catch (Exception ex)
            {
            }
        }



        #endregion 


        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (propertyName != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
