﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Cocktail_App.DataBase;
using Cocktail_App.Models;
using Cocktail_App.Views;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Realms;
using Xamarin.Forms;

namespace Cocktail_App.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {

        private User _user = new User();
        private bool _agree = false;

        private String _newphoto = "";

        public User user
        {
            get
            {
                return _user;
            }
            set
            {
                _user = value;
                OnPropertyChanged("user");
            }
        }

        public Boolean agree
        {
            get
            {
                return _agree;
            }
            set
            {
                _agree = value;
                OnPropertyChanged("agree");
            }
        }



        #region Comandos

        public ICommand LoginCommand { get; set; }
        public ICommand GoSignUpCommand { get; set; }
        public ICommand BackLoginCommand { get; set; }
        public ICommand SignUpCommand { get; set; }
        public ICommand CheckSessionCommand { get; set; }
        public ICommand CheckTermsCommand { get; set; }
        public ICommand LogOutCommand { get; set; }
        public ICommand SaveProfileCommand { get; set; }
        public ICommand ChangePhotoCommand { get; set; }

        #endregion

        private static LoginViewModel instance = null;

        public static LoginViewModel GetInstance()
        {
            if (instance == null)
            {
                instance = new LoginViewModel();
            }

            return instance;
        }

        public LoginViewModel()
        {
            InitCommands();

        }

        public void InitCommands()
        {
            GoSignUpCommand = new Command(async () => await Shell.Current.GoToAsync("//SignUpPage"));
            BackLoginCommand = new Command(async () => await Shell.Current.GoToAsync("//LoginPage"));
            LoginCommand = new Command(Login);
            SignUpCommand = new Command(SignUp);
            CheckSessionCommand = new Command(CheckSession);
            CheckTermsCommand = new Command(CheckTerms);
            LogOutCommand = new Command(LogOut);
            SaveProfileCommand = new Command(SaveProfile);
            ChangePhotoCommand = new Command(ChangePhoto);
        }

        private async void ChangePhoto()
        {
            try
            {
                var photo = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    SaveToAlbum = true,
                    SaveMetaData = true
                });
                if (photo == null)
                {
                    return;
                }

                var realm = Realm.GetInstance();
                using (var trans = realm.BeginWrite())
                {
                    user.StrImage = photo.AlbumPath;
                    trans.Commit();
                }
                OnPropertyChanged("user");

            } catch(NotSupportedException ex)
            {
                await Shell.Current.DisplayAlert("Open Camera", "Camera is not supported in this device", "OK");
            } catch (Exception ex)
            {
                await Shell.Current.DisplayAlert("Open Camera", ex.Message, "OK");
            }
        }

        private async void SaveProfile()
        {
            try
            {

                if (ValidarRegistro("P").Equals(""))
                {
                    var action = await Application.Current.MainPage.DisplayAlert("Edit Profile", "Confirm to update your information...", "Confirm", "Dismiss");
                    if (action)
                    {
                        var realm = Realm.GetInstance();
                        using (var trans = realm.BeginWrite())
                        {

                            trans.Commit();
                        }
                        OnPropertyChanged("user");
                        await Application.Current.MainPage.DisplayAlert("Edit Profile", "Profile updated successfully", "OK");
                    }
                }

                else
                {
                    await Shell.Current.DisplayAlert("Save Profile", "Please verify your data:\n\n" + ValidarRegistro("P"), "OK");
                }
            }

            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Oops! An error has ocurred:\n\n" + ex.Message, "OK");
            }
        }

        private async void LogOut()
        {
            try
            {
                var action = await Application.Current.MainPage.DisplayAlert("Log Out", "Confirm to log out...", "Log Out", "Dismiss");
                if (action)
                {
                    var realm = Realm.GetInstance();
                    using (var trans = realm.BeginWrite())
                    {

                        user.Remember = false;

                        trans.Commit();
                    }
                    user = new User();
                    OnPropertyChanged("user");
                    await Shell.Current.GoToAsync("//LoginPage");
                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Oops! An error has ocurred:\n\n" + ex.Message, "OK");
            }

        }
        private async void Login()
        {

            try
            {
                if (ValidarLogin().Equals(""))
                {


                    var realm = Realm.GetInstance();

                    User bdUser = realm.All<User>().Where(u => u.Email == user.Email).FirstOrDefault();

                    if (bdUser == null)
                    {
                        await Application.Current.MainPage.DisplayAlert("Log In", "User was not found.\n\nWould you like to register to Cocktail App? " +
                            "Tap Sign Up to create an account", "OK");
                    }
                    else
                    {

                        if (user.Password == bdUser.Password)
                        {

                            using (var trans = realm.BeginWrite())
                            {
                                bdUser.Session = true;
                                bdUser.Remember = user.Remember;
                                bdUser.Email = user.Email;
                                trans.Commit();
                            }
                            user = bdUser;
                            OnPropertyChanged("user");
                            await Shell.Current.GoToAsync("//MainPage");
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Log In", "Incorrect password.\n\nPlease check your password and try again", "OK");
                        }
                    }

                }
                else
                {
                    await Shell.Current.DisplayAlert("Log In", "Please verify your data:\n\n" + ValidarLogin(), "OK");
                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Oops! An error has ocurred:\n\n" + ex.Message, "OK");
            }


        }

        private async void SignUp()
        {
            try
            {
                if (ValidarRegistro("S").Equals(""))
                {
                    var realm = Realm.GetInstance();

                    var new_user = realm.All<User>().Where(u => u.Email == user.Email).FirstOrDefault();

                    if (new_user == null)
                    {
                        realm.Write(() =>
                        {
                            realm.Add(user);
                        });

                        user = new User();
                        agree = false;

                        await Shell.Current.DisplayAlert("Sign Up", "Registration successful!\n\nGo to Log In", "OK");
                        await Shell.Current.GoToAsync("//LoginPage");

                    }
                    else
                    {
                        user = new User();
                        agree = false;
                        await Shell.Current.DisplayAlert("Sign Up", "You already have an account in Cocktail App\n\nGo to Log In", "OK");
                        await Shell.Current.GoToAsync("//LoginPage");
                    }
                }
                else
                {
                    await Shell.Current.DisplayAlert("Sign Up", "Please verify your data:\n\n" + ValidarRegistro("S"), "OK");
                }
            }
            catch (Exception ex)
            {
                await Shell.Current.DisplayAlert("Error", "Oops! An error has ocurred:\n\n" + ex.Message, "OK");
                await Shell.Current.GoToAsync("//LoginPage");
            }

        }

        private void CheckSession()
        {
            if (user.Remember)
            {
                user.Remember = false;
            }
            else
            {
                user.Remember = true;
            }

            OnPropertyChanged("user");
        }

        private void CheckTerms()
        {
            if (agree)
            {
                agree = false;
            }
            else
            {
                agree = true;
            }
        }

        public String ValidarRegistro(string tipo)
        {
            String msj = "";

            if(user.StrImage == null)
            {
                user.StrImage = "default_user_icon.png";
            }
            if (user.Fullname == null)
            {
                msj += "Enter Fullname\n";
            }

            if (user.Email == null)
            {
                msj += "Enter Email\n";
            }
            else if (!user.Email.Contains("@"))
            {
                msj += "Enter a valid Email\n";
            }
            if (user.Password == null)
            {
                msj += "Enter Password\n";
            }

            if (tipo.Equals("S"))
            {
                if (!agree)
                {
                    msj += "You must accept Terms of Services and Privacy Policy\n";
                }
            }

            return msj;
        }

        public String ValidarLogin()
        {
            String msj = "";

            if (user.Email == null)
            {
                msj += "Enter Email\n";
            }
            else if (!user.Email.Contains("@"))
            {
                msj += "Enter a valid Email\n";
            }

            if (user.Password == null)
            {
                msj += "Enter Password\n";
            }

            return msj;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (propertyName != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
